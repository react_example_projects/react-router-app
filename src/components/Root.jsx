import { Outlet } from 'react-router-dom';

import MainNavigation from './MainNavigation';

export default function RootLayOut() {
  return (
    <>
      <MainNavigation />
      <main>
        <Outlet />
      </main>
    </>
  );
}
