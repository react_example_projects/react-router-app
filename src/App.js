import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './components/HomePage';
import ProductPage from './components/ProductPage';
import RootLayOut from './components/Root';
import ErrorPage from './components/ErrorPage';
import ProductDetailPage from './components/ProductDetailPage';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayOut />,
    children: [
      { index: true, element: <HomePage /> },
      { path: 'products', element: <ProductPage /> },
      { path: 'products/:id', element: <ProductDetailPage /> },
    ],
    errorElement: <ErrorPage />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
